#include "number.h"

//empty
number_t::number_t() {}

//from number
number_t::number_t(int64_t n) : a{n} {}

//copy constructor
number_t::number_t(const number_t &num) : a{num.a} {}

void number_t::swap(number_t &num) {
    std::swap(a, num.a);
}

// operator =
number_t& number_t::operator=(const number_t &num) {
    number_t cop = num;
    swap(cop);
    return *this;
}

// casts
number_t::operator bool() const{
    return a!=0;  //ATTENTION
}

number_t::operator int64_t() const {
    return a;
}

number_t::operator std::string() const {
    return std::to_string(a);
}

// unary op-s
number_t number_t::operator+() const {
    return a;
}

number_t number_t::operator-() const {
    return -a;
}

number_t number_t::operator!() const {
    return !a;
}

number_t number_t::operator~() const {
    return ~a;
}

// binary arifm op-s
number_t operator+(const number_t &left_num, const number_t &right_num) {
    return left_num.a + right_num.a;
}

number_t operator-(const number_t &left_num, const number_t &right_num) {
    return left_num.a - right_num.a;
}

number_t operator*(const number_t &left_num, const number_t &right_num) {
    return left_num.a * right_num.a;
}

number_t operator/(const number_t &left_num, const number_t &right_num) {
    return left_num.a / right_num.a;
}

number_t operator%(const number_t &left_num, const number_t &right_num) {
    return left_num.a % right_num.a;
}

number_t &operator+=(number_t &left_num, const number_t &right_num) {
    left_num.a += right_num.a;
    return left_num;
}

number_t &operator-=(number_t &left_num, const number_t &right_num) {
    left_num.a -= right_num.a;
    return left_num;
}

number_t &operator*=(number_t &left_num, const number_t &right_num) {
    left_num.a *= right_num.a;
    return left_num;
}

number_t &operator/=(number_t &left_num, const number_t &right_num) {
    left_num.a /= right_num.a;
    return left_num;
}

number_t &operator%=(number_t &left_num, const number_t &right_num) {
    left_num.a %= right_num.a;
    return left_num;
}

// bin bit op-s
number_t operator^(const number_t &left_num, const number_t &right_num) {
    return left_num.a ^ right_num.a;
}

number_t operator&(const number_t &left_num, const number_t &right_num) {
    return left_num.a & right_num.a;
}

number_t operator|(const number_t &left_num, const number_t &right_num) {
    return left_num.a | right_num.a;
}

number_t &operator^=(number_t &left_num, const number_t &right_num) {
    left_num.a ^= right_num.a;
    return left_num;
}

number_t &operator&=(number_t &left_num, const number_t &right_num) {
    left_num.a &= right_num.a;
    return left_num;
}

number_t &operator|=(number_t &left_num, const number_t &right_num) {
    left_num.a |= right_num.a;
    return left_num;
}

// bin shift op-s
number_t operator<<(const number_t &left_num, const number_t &right_num) {
    return left_num.a << right_num.a;
}

number_t operator>>(const number_t &left_num, const number_t &right_num) {
    return left_num.a >> right_num.a;
}

number_t &operator<<=(number_t &left_num, const number_t &right_num) {
    left_num.a <<= right_num.a;
    return left_num;
}

number_t &operator>>=(number_t &left_num, const number_t &right_num) {
    left_num.a >>= right_num.a;
    return left_num;
}

// increment and decrement prefix and postfix op-s
number_t& number_t::operator++() {
    return  *this += 1;
}

number_t number_t::operator++(int) {
    number_t cop = *this;
    ++*this;
    return cop;
}

number_t& number_t::operator--() {
    return  *this -= 1;
}

number_t number_t::operator--(int) {
    number_t cop = *this;
    --*this;
    return cop;
}

// output and input op-s
std::ostream &operator<<(std::ostream &out_str, const number_t &num) {
    out_str<<num.a;
    return out_str;
}

std::istream &operator>>(std::istream &in_str, number_t &num) {
    in_str>>num.a;
    return in_str;
}

// coparators
bool operator==(const number_t &left, const number_t &right) {
    return (left.a == right.a);
}

bool operator!=(const number_t &left, const number_t &right) {
    return !(left.a == right.a);
}

bool operator<(const number_t &left, const number_t &right) {
    return left.a < right.a;
}

bool operator>=(const number_t &left, const number_t &right) {
    return !(left.a < right.a);
}

bool operator>(const number_t &left, const number_t &right) {
    return left.a > right.a;
}

bool operator<=(const number_t &left, const number_t &right) {
    return !(left.a > right.a);
}
