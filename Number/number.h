#pragma once

#include <cstdint>
#include <iostream>

class number_t {
private:
    int64_t a{0};
    //int a{0};
public:
    number_t();
    number_t(int64_t n);
    number_t(const number_t& num);
    void swap(number_t &num);
    number_t &operator=(const number_t &num);

    explicit operator bool() const;
    explicit operator int64_t() const;
    explicit operator std::string() const;

    number_t operator+() const;
    number_t operator-() const;
    number_t operator!() const;
    number_t operator~() const;

    friend number_t operator+(const number_t &left_num, const number_t &right_num);
    friend number_t operator-(const number_t &left_num, const number_t &right_num);
    friend number_t operator*(const number_t &left_num, const number_t &right_num);
    friend number_t operator/(const number_t &left_num, const number_t &right_num);
    friend number_t operator%(const number_t &left_num, const number_t &right_num);
    friend number_t &operator+=(number_t &left_num, const number_t &right_num);
    friend number_t &operator-=(number_t &left_num, const number_t &right_num);
    friend number_t &operator*=(number_t &left_num, const number_t &right_num);
    friend number_t &operator/=(number_t &left_num, const number_t &right_num);
    friend number_t &operator%=(number_t &left_num, const number_t &right_num);

    friend number_t operator^(const number_t &left_num, const number_t &right_num);
    friend number_t operator&(const number_t &left_num, const number_t &right_num);
    friend number_t operator|(const number_t &left_num, const number_t &right_num);
    friend number_t &operator^=(number_t &left_num, const number_t &right_num);
    friend number_t &operator&=(number_t &left_num, const number_t &right_num);
    friend number_t &operator|=(number_t &left_num, const number_t &right_num);

    friend number_t operator<<(const number_t &left_num, const number_t &right_num);
    friend number_t operator>>(const number_t &left_num, const number_t &right_num);
    friend number_t &operator<<=(number_t &left_num, const number_t &right_num);
    friend number_t &operator>>=(number_t &left_num, const number_t &right_num);

    number_t& operator++();
    number_t operator++(int);
    number_t& operator--();
    number_t operator--(int);

    friend std::ostream &operator<<(std::ostream &out_str, const number_t &num);
    friend std::istream &operator>>(std::istream &in_str, number_t &num);

    friend bool operator==(const number_t &left, const number_t &right);
    friend bool operator!=(const number_t &left, const number_t &right);
    friend bool operator<(const number_t &left, const number_t &right);
    friend bool operator>=(const number_t &left, const number_t &right);
    friend bool operator>(const number_t &left, const number_t &right);
    friend bool operator<=(const number_t &left, const number_t &right);
};
