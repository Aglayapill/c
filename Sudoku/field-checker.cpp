#include "field-checker.h"

bool check_field(const std::vector<std::vector<int>> &init_field, const std::vector<std::vector<int>> &solution) {

  std::vector<int> ob = {1,1,1,1,1,1,1,1,1};

  // rows and cols
  for (int r=0; r<9; r++) {
    std::vector<int> samp_r = {0,0,0,0,0,0,0,0,0};
    std::vector<int> samp_c = {0,0,0,0,0,0,0,0,0};

    for (int c=0; c<9; c++) {
      if (solution[r][c] != 0) {
        int v_r = solution[r][c];
        v_r -= 1;
        samp_r[v_r] += 1;
      }

      if (solution[c][r] != 0) {
        int v_c = solution[c][r];
        v_c -= 1;
        samp_c[v_c] += 1;
      }
    }
    if (samp_r != ob) {
      return false;
    }

    if (samp_c != ob) {
      return false;
    }
  }

  // squares

  int r = 0;
  int c = 0;

  while (r<9) {
    while (c<9) {


      std::vector<int> samp_s = {0, 0, 0, 0, 0, 0, 0, 0, 0};
      for (int i = r; i <r+3; ++i) {
        for (int j = c; j < c+3; ++j) {
          if (solution[i][j] != 0) {
            int v_s = solution[i][j];
            v_s -= 1;
            samp_s[v_s] += 1;
          }
        }
      }

      if (samp_s != ob) {
        return false;
      }

      c += 3;

    }
    c = 0;
    r += 3;

  }

  return true;

}
