#include "sudoku-solver.h"
#include "field-checker.h"

// how many zeros
bool zerozz_search(const std::vector<std::vector<int>> &field) {

  std::vector<std::vector<int>> zero_vec;
  for (int i=0; i<9; i++) {
    for (int j=0; j<9; j++) {
      if (field[i][j]==0) {
        zero_vec.push_back({i,j});
      }
    }
  }

  if (zero_vec.size()==1){
    return true;
  } else {
    return false;
  }

}

// search for the first zero
std::vector<int> zero_search(const std::vector<std::vector<int>> &field) {
  std::vector<int> zero_vec;
  int fl = 0;
  for (int i = 0; i < 9; i++) {
    for (int j = 0; j < 9; j++) {
      if (field[i][j] == 0) {
        zero_vec.push_back(i);
        zero_vec.push_back(j);
        fl = 1;
        break;
      }
    }
    if (fl ==1) {
      break;
    }
  }
  return zero_vec;
}

// possible values
std::vector<int> search_val(const std::vector<std::vector<int>> &field, int r, int c) {

  std::vector<int> all_val = {0,0,0,0,0,0,0,0,0};
  int val =0;

  // for row and col
  for (int j=0; j<9; j++) {
    val = field[r][j];
    if (val != 0) {
      val -= 1;
      all_val[val] = 1;
    }
    val = field[j][c];
    if (val != 0) {
      val -= 1;
      all_val[val] = 1;
    }
  }


  // for square
  int rd = r - r%3;
  int ru = rd +3;
  int cd = c - c%3;
  int cu = cd + 3;

  for (int i=rd; i<ru; i++) {
    for (int j=cd; j<cu; j++) {
      val = field[i][j];
      if (val != 0) {
        val -= 1;
        all_val[val] = 1;
      }
    }
  }

  // converting into 1..9 numbers
  std::vector<int> res_val;
  for (int i=0; i<9; i++) {
    if (all_val[i] == 0) {
      val = i+1;
      res_val.push_back(val);
    }
  }

  return res_val;

}

// solving
size_t n = 0;
std::pair<size_t, std::vector<std::vector<int>>> sudoku_solve(const std::vector<std::vector<int>> &field) {


  std::pair<size_t, std::vector<std::vector<int>>> p;
  std::vector<std::vector<int>> field1=field;


  std::vector<int> zeros = zero_search(field1);
  if (zeros.empty()) {
    p.first = n;
    p.second = field1;
    return p;
  }

  int a =zeros[0];
  int b = zeros[1];

  std::vector<int> pv = search_val(field1,a,b);

    for (int i = 0; i < pv.size(); i++) {
      field1[a][b] = pv[i];

      std::pair<size_t, std::vector<std::vector<int>>> pp;
      pp = sudoku_solve(field1);
      std::vector<std::vector<int>> solu = pp.second;

      if (check_field(field1, solu)) {
        p.second = solu;
        if (zerozz_search(field)) {
          n += 1;
        }
        p.first = n;
      }
    }

  if (p.second.empty()) {
    field1[a][b] = 0;
    p.second = field1;
  }

  p.first = n;
  return p;

}
