#include "phone-book.h"

bool phone_book_t::create_user(const std::string &number, const std::string &name) {
  std::set<std::string> all_nums;
  std::vector<user_t>::iterator it = user_struct.begin();
  for (;it != user_struct.end(); it++) {
    all_nums.insert((*it).number);
  }
  if (all_nums.count(number)) {
    return false;
  } else {
    user_t new_user(number, name);
    user_struct.push_back(new_user);

    user_info_t new_one;
    new_one.user = new_user;
    new_one.total_call_duration_s = 0;
    user_inf_struct.push_back(new_one);

    return true;
  }
}

bool phone_book_t::add_call(const call_t &call) {
  std::set<std::string> all_nums;
  std::vector<user_t>::iterator it = user_struct.begin();
  for (;it != user_struct.end(); it++) {
    all_nums.insert((*it).number);
  }
  if (all_nums.count(call.number)) {
    call_struct.push_back(call);

    std::vector<user_info_t>::iterator it = user_inf_struct.begin();
    for (;it != user_inf_struct.end(); it++) {
      if (call.number == (*it).user.number) {
        (*it).total_call_duration_s += call.duration_s;
      }
    }

    return true;
  } else {
    return false;
  }
}

std::vector<call_t> phone_book_t::get_calls(size_t start_pos, size_t count) const {
  std::vector<call_t> call_list;
  if (start_pos > call_struct.size()) {
    return call_list;
  } else {
    size_t c = 0;
    for (auto i = call_struct.begin() + start_pos; i != call_struct.end(); i++) {
      if (i == call_struct.end()) {
        break;
      }
      if (c == count) {
        break;
      }
      call_list.push_back(*i);
      c++;
    }
  }
  return call_list;
}



bool num_comparator(const user_info_t &comp1, const user_info_t &comp2) {

  if (comp1.total_call_duration_s != comp2.total_call_duration_s) {
    return comp1.total_call_duration_s > comp2.total_call_duration_s;
  }

  if (comp1.user.name != comp2.user.name) {
    return comp1.user.name < comp2.user.name;
  }

  if (comp1.user.number == "" || comp2.user.number == "") {
    return comp1.user.number.size() < comp2.user.number.size();
  } else {
    return comp1.user.number > comp2.user.number && comp1.user.number.size() < comp2.user.number.size();
  }
}


std::vector<user_info_t> phone_book_t::search_users_by_number(const std::string &number_prefix, size_t count) const {

  std::vector<user_info_t> res;

  if (number_prefix == "" && count==0) {
    return res;
  }

  //std::vector<user_info_t>::iterator it = user_inf_struct.begin();
  for (auto it = user_inf_struct.begin() ;it != user_inf_struct.end(); it++) {
    if ((*it).user.number.substr(0, number_prefix.size()) == number_prefix) {
      res.push_back(*it);
    }
  }

  if (res.empty()) {
    return res;
  }
  std::sort(res.begin(), res.end(), num_comparator);

  std::vector<user_info_t> res_c;
  std::vector<user_info_t>::iterator it = res.begin();
  for (int i = 0; i < count; i++) {
    res_c.push_back(*it);
    it++;
    if (it == res.end()) {
      break;
    }
  }
  return res_c;
}

bool name_comparator(const user_info_t &comp1, const user_info_t &comp2) {

  if (comp1.user.name != comp2.user.name) {
    return comp1.user.name < comp2.user.name;
  }

  if (comp1.total_call_duration_s != comp2.total_call_duration_s) {
    return comp1.total_call_duration_s > comp2.total_call_duration_s;
  }

  if (comp1.user.number == "" || comp2.user.number == "") {
    return comp1.user.number.size() < comp2.user.number.size();
  } else {
    return comp1.user.number < comp2.user.number && comp1.user.number.size() < comp2.user.number.size();
  }
}

std::vector<user_info_t> phone_book_t::search_users_by_name(const std::string &name_prefix, size_t count) const {

  std::vector<user_info_t> res;

  if (name_prefix == "" && count==0) {
    return res;
  }

  for (auto it = user_inf_struct.begin() ;it != user_inf_struct.end(); it++) {
    if ((*it).user.name.substr(0, name_prefix.size()) == name_prefix) {
      res.push_back(*it);
    }
  }


  if (res.empty()) {
    return res;
  }
  std::sort(res.begin(), res.end(), name_comparator);

  std::vector<user_info_t> res_c;
  std::vector<user_info_t>::iterator it = res.begin();
  for (int i = 0; i < count; i++) {
    res_c.push_back(*it);
    it++;
    if (it == res.end()) {
      break;
    }
  }
  return res_c;
}

void phone_book_t::clear() {
  call_struct.clear();
  user_struct.clear();
  user_inf_struct.clear();
}

size_t phone_book_t::size() const {
  return user_struct.size();
}

bool phone_book_t::empty() const {
  return user_struct.empty() && user_inf_struct.empty() && call_struct.empty();
}